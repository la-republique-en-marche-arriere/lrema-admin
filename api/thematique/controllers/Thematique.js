'use strict';

/**
 * Thematique.js controller
 *
 * @description: A set of functions called "actions" for managing `Thematique`.
 */

module.exports = {

  /**
   * Retrieve thematique records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.thematique.search(ctx.query);
    } else {
      return strapi.services.thematique.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a thematique record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.thematique.fetch(ctx.params);
  },

  /**
   * Count thematique records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.thematique.count(ctx.query);
  },

  /**
   * Create a/an thematique record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.thematique.add(ctx.request.body);
  },

  /**
   * Update a/an thematique record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.thematique.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an thematique record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.thematique.remove(ctx.params);
  }
};

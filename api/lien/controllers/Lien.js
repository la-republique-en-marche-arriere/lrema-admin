'use strict';

/**
 * Lien.js controller
 *
 * @description: A set of functions called "actions" for managing `Lien`.
 */

module.exports = {

  /**
   * Retrieve lien records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.lien.search(ctx.query);
    } else {
      return strapi.services.lien.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a lien record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.lien.fetch(ctx.params);
  },

  /**
   * Count lien records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.lien.count(ctx.query);
  },

  /**
   * Create a/an lien record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.lien.add(ctx.request.body);
  },

  /**
   * Update a/an lien record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.lien.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an lien record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.lien.remove(ctx.params);
  }
};
